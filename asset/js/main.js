$('#video01').hoverPlay({
  callbacks: {
    play: function(el, video) {
      video.play();
      el.addClass('hoverPlay');
      $('.video-01').removeClass('back-video-01');
    },
    pause: function(el, video) {
      video.pause();
      video.currentTime = 0;
      el.removeClass('hoverPlay');
      $('.video-01').addClass('back-video-01');
    },
    click: function(el, video, e) {
      e.preventDefault();
    }
  }
});

$('#video02').hoverPlay({
  callbacks: {
    play: function(el, video) {
      video.play();
      el.addClass('hoverPlay');
      $('.video-02').removeClass('back-video-02');
    },
    pause: function(el, video) {
      video.pause();
      video.currentTime = 0;
      el.removeClass('hoverPlay');
      $('.video-02').addClass('back-video-02');
    },
    click: function(el, video, e) {
      e.preventDefault();
    }
  }
});

$('#video03').hoverPlay({
  callbacks: {
    play: function(el, video) {
      video.play();
      el.addClass('hoverPlay');
      $('.video-03').removeClass('back-video-03');
    },
    pause: function(el, video) {
      video.pause();
      video.currentTime = 0;
      el.removeClass('hoverPlay');
      $('.video-03').addClass('back-video-03');
    },
    click: function(el, video, e) {
      e.preventDefault();
    }
  }
});

$('#video04').hoverPlay({
  callbacks: {
    play: function(el, video) {
      video.play();
      el.addClass('hoverPlay');
      $('.video-04').removeClass('back-video-04');
    },
    pause: function(el, video) {
      video.pause();
      video.currentTime = 0;
      el.removeClass('hoverPlay');
      $('.video-04').addClass('back-video-04');
    },
    click: function(el, video, e) {
      e.preventDefault();
    }
  }
});

function MouseWheelHandler(e, element) {
    var delta = 0;
    if (typeof e === 'number') {
        delta = e;
    } else {
        if (e.deltaX !== 0) {
            delta = e.deltaX;
        } else {
            delta = e.deltaY;
        }
        e.preventDefault();
    }

    element.scrollLeft -= (delta);

}
/*
window.onload = function() {
    var carousel = {};
    carousel.e = document.getElementById('carousel');
    carousel.items = document.getElementById('carousel-items');
    carousel.leftScroll = document.getElementById('left-scroll-button');
    carousel.rightScroll = document.getElementById('right-scroll-button');

    //carousel.items.addEventListener("mousewheel", handleMouse, false);
    //carousel.items.addEventListener("scroll", scrollEvent);
    //carousel.leftScroll.addEventListener("click", leftScrollClick);
    //carousel.rightScroll.addEventListener("click", rightScrollClick);
    /* carousel.leftScroll.addEventListener("mousedown", leftScrollClick);
     carousel.rightScroll.addEventListener("mousedown", rightScrollClick);

    setLeftScrollOpacity();
    setRightScrollOpacity();

    function handleMouse(e) {
        MouseWheelHandler(e, carousel.items);
    }

    function leftScrollClick() {
        MouseWheelHandler(100, carousel.items);
    }

    function rightScrollClick() {
        MouseWheelHandler(-100, carousel.items);
    }

    function scrollEvent(e) {
        setLeftScrollOpacity();
        setRightScrollOpacity();
    }

    function setLeftScrollOpacity() {
        if (isScrolledAllLeft()) {
            carousel.leftScroll.style.opacity = 0;
        } else {
            carousel.leftScroll.style.opacity = 1;
        }
    }

    function isScrolledAllLeft() {
        if (carousel.items.scrollLeft === 0) {
            return true;
        } else {
            return false;
        }
    }

    function isScrolledAllRight() {
        if (carousel.items.scrollWidth > carousel.items.offsetWidth) {
            if (carousel.items.scrollLeft + carousel.items.offsetWidth === carousel.items.scrollWidth) {
                return true;
            }
        } else {
            return true;
        }

        return false;
    }

    function setRightScrollOpacity() {
        if (isScrolledAllRight()) {
            carousel.rightScroll.style.opacity = 0;
        } else {
            carousel.rightScroll.style.opacity = 1;
        }
    }
}
*/