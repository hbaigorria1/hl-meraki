<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
				
			 <form method="post" action="<?php echo base_url()?>categorias/update/<?php echo $info[0]->{'id'} ?>/">
			 	<div class="row">
			 		<div class="col-xs-12 col-md-6">
			 			<div class="td-input">
			 				<b>Nombre:</b><br>
			 				<input type="text" name="nombre" id="nombre" value="<?php echo $info[0]->nombre ?>">
			 			</div>
			 		</div>
			 		<div class="col-xs-12 col-md-6">
			 			<div class="td-input">
			 				<b>Seleccionar catalogo:</b><br>
			 				<select name="id_catalogo">
			 					<option>Seleccionar...</option>
			 					<?php foreach($catalogos as $catalogo): ?>
			 						<option value="<?=$catalogo->id?>" <?php if($catalogo->id == $info[0]->id_catalogo): echo "selected"; endif; ?>><?=$catalogo->nombre?></option>
			 					<?php endforeach; ?>
			 				</select>
			 			</div>
			 		</div>
			 	</div>
			 	<div class="row">
			 		<div class="col-xs-12 col-md-6">
						<div class="td-input">
							<b>Imagen portada:</b><br>
							<input type="text" name="galeria1_input" id="galeria1_input" class="img-input" value="<?php echo $info[0]->portada ?>" readonly>
							<div id="main_uploader">
								<div class="uploader-id1">
									<div id="uploader1" align="left">
										<input id="uploadify1" type="file" class="uploader" />
									</div>
								</div>
								<div id="filesUploaded" style="display:none;"></div>
								<div id="thumbHeight1" style="display:none;" >800</div>
								<div id="thumbWidth1" style="display:none;" >1131</div>
							</div>
							<div id="galeria1" class="upload-galeria">
								<?php if($info[0]->{'portada'}<>''){ ?>
								<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'portada'}?>',function(){}); $('#galeria1_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[0]->{'portada'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="td-input">
							<b>Imagen footer:</b><br>
							<input type="text" name="galeria2_input" id="galeria2_input" class="img-input" value="<?php echo $info[0]->footer ?>" readonly>
							<div id="main_uploader">
								<div class="uploader-id2">
									<div id="uploader2" align="left">
										<input id="uploadify2" type="file" class="uploader" />
									</div>
								</div>
								<div id="filesUploaded" style="display:none;"></div>
								<div id="thumbHeight2" style="display:none;" >800</div>
								<div id="thumbWidth2" style="display:none;" >1131</div>
							</div>
							<div id="galeria2" class="upload-galeria">
								<?php if($info[0]->{'footer'}<>''){ ?>
								<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'footer'}?>',function(){}); $('#galeria2_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[0]->{'footer'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>categorias/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>