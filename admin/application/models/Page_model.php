<?php

class page_model extends CI_Model
{


		public function __construct()
        {
			parent::__construct();
			// Your own constructor code
        }
		
		
		
        public function login($data) {

			$condition = "user_login.user_name =" . "'" . $data['username'] . "' AND " . "user_login.user_password =" . "'" . $data['password'] . "'";
			$this->db->select('user_login.*, provincia.provincia');
			$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}
		public function checkuser($user) {

			$condition = "user_name =" . "'" . $user . "'";
			$this->db->select('*');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}

			// Read data from database to show data in admin page
		public function read_user_information($username) {
			$condition = "user_name =" . "'" . $username . "'";
			$this->db->select('user_login.*, provincia.provincia');
			$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return $query->result();
			} else {
				return false;
			}
		}
			
        
		public function get_usuarios()
        {
				$this->db->select('user_login.*, provincia.provincia');
				$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
                $query = $this->db->get('user_login');
                return $query->result();
        }
		public function get_usuarios_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('user_login');
                return $query->result();
        }


        public function get_catalogos()
        {
            $query = $this->db->get('catalogos');
            return $query->result();
        }


        public function get_paises()
        {
            $query = $this->db->get('countries');
            return $query->result();
        }


        public function get_categorias()
        {
        	$this->db->select('categorias.*');
        	$this->db->select('catalogos.nombre as catalogo_nombre');
        	$this->db->join('catalogos', 'catalogos.id = categorias.id_catalogo');
            $query = $this->db->get('categorias');	
            return $query->result();
        }

        public function get_productos()
        {
            $query = $this->db->get('productos');  
            return $query->result();
        }

        public function get_subcategorias($id_categoria)
        {
        	$this->db->select('subcategorias.*');
        	$this->db->select('categorias.nombre as categoria');
        	$this->db->join('categorias', 'categorias.id = subcategorias.id_categoria');
        	$this->db->where('id_categoria', $id_categoria);
            $query = $this->db->get('subcategorias');	
            return $query->result();
        }


        public function get_catalogo_id($id)
        {
        	$this->db->where('id', $id);
        	$query = $this->db->get('catalogos');
        	return $query->result();
        }

        public function get_pais_id($id)
        {
        	$this->db->where('id', $id);
        	$query = $this->db->get('countries');
        	return $query->result();
        }

        public function get_categoria_id($id)
        {
        	$this->db->where('id', $id);
        	$query = $this->db->get('categorias');
        	return $query->result();
        }


        public function get_producto_id($id)
        {
            $this->db->where('id', $id);
            $query = $this->db->get('productos');
            return $query->result();
        }

        public function get_subcategoria_id($id)
        {
        	$this->db->where('id', $id);
        	$query = $this->db->get('subcategorias');
        	return $query->result();
        }
		

		/*INSERTS*/
		
		
		public function insert_usuario($data)
        {
            $this->db->insert('user_login', $data);
        }

        public function insert_catalogo($data)
        {
            $this->db->insert('catalogos', $data);
        }

        public function insert_pais($data)
        {
            $this->db->insert('countries', $data);
        }

        public function insert_categoria($data)
        {
            $this->db->insert('categorias', $data);
        }

        public function insert_producto($data)
        {
            $this->db->insert('productos', $data);
        }

        public function insert_subcategoria($data)
        {
            $this->db->insert('subcategorias', $data);
        }
        
		
		/*UPDATES*/

		public function update_catalogo($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('catalogos', $data);
        }

        public function update_pais($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('countries', $data);
        }

        public function update_categoria($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('categorias', $data);
        }

        public function update_producto($data)
        {
            $this->db->where('id', $this->uri->segment(3));
            $this->db->update('productos', $data);
        }

        public function update_subcategoria($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('subcategorias', $data);
        }

		public function update_usuario($data)
        {
				$this->db->set('name', $data['name']);
				$this->db->set('provinciaId', $data['provinciaId']);
				$this->db->set('vendedor_id', $data['vendedor_id']);
				$this->db->set('administrator', $data['administrator']);
				$this->db->set('lastname', $data['lastname']);
				$this->db->set('user_email', $data['user_email']);
				if($data['user_password']<>''){
					$this->db->set('user_password', $data['user_password']);
				}
				$this->db->where('id', $this->uri->segment(3));
                $this->db->update('user_login');
        }
		
		
		/*REMOVES*/
			
		public function remove_catalogo()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('catalogos');
        }

        public function remove_pais()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('countries');
        }

        public function remove_categoria()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('categorias');
        }

        public function remove_subcategoria()
        {
                $this->db->where('id', $this->uri->segment(3));
                $this->db->delete('subcategorias');
        }


        public function remove_producto()
        {
                $this->db->where('id', $this->uri->segment(3));
                $this->db->delete('productos');
        }
        
		public function remove_usuario()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('user_login');
        }
        
}

?>