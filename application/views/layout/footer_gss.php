<footer id="contacto">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4">
				<h3><?=$this->lang->line('title_contacto')?></h3>
				<p><?=$this->lang->line('description_contacto')?></p>
			</div>
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-6">
				<iframe id="pardot_gated" src="<?=$this->lang->line('form_04')?>" width="100%" height="800" type="text/html" frameborder="0" allowtransparency="true" style="border: 0;height: 830px;"></iframe>
			</div>
			<div class="col-12 text-center">
				<p style="margin:0 0 30px;font-size:17px;"><?=$this->lang->line('desc_foot_gss')?></p>
			</div>
		</div>
	</div>
	<div class="row w-100 backfootcolor">
		<div class="col-12 text-center">
			<a href="#">
				<img src="https://addtest.live/descubrehillrom/asset/img/LogoBaxter-01.png" class="img-fluid">
			</a>
		</div>
	</div>
	
</footer>