<style type="text/css">.menu-slider{margin-top:0px;}.novedades .contante-novedad p {font-size:29px;}.novedades .contante-novedad{padding:10px 25px 45px}</style>

	<style type="text/css">
		.novedades .contante-novedad {min-height:470px;}
		.image-alternative.cust-image-alternative {height:280px;}
	</style>

<section class="back-home res-section d-flex align-items-center" style="background-image:url('<?=base_url()?>asset/img/back-flc.png');">
	<video id="videoheader" src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_FLC Video_Loop_1.mp4" loop muted autoplay>
	</video>
	<div class="absolute-icon">
		<img src="<?=base_url()?>asset/img/play-icon.png" class="img-fluid play-home">
	</div>
	<div class="filter-01"></div>
	<div class="filter-02"></div>
	<div class="w-100 header-section" style="position:relative;">
		<div class="row">
			<div class="box-header-section">
				<h3><?=$this->lang->line('diagnostico_title')?></h3>
			</div>
		</div>
	</div>
	<div class="modal-video">
		<div class="container" style="position:relative;">
			<div class="row">	
				<div class="col-12 col-md-12">
						<video id="videoId" src="<?=base_url()?>asset/video/<?=$this->lang->line('video_03')?>" controls loop muted autoplay></video>
						<div class="close-modal">X</div>
				</div>
	    </div>
    </div>
	</div>
</section>

<section class="secciones menu">
	<div class="content-menu m-0 justify-content-center">
		
			<a href="#soluciones" class="box-menu">
				<p><?=$this->lang->line('menu_section_01')?></p>
			</a>
		
			<a href="#novedad" class="box-menu">
				<p><?=$this->lang->line('menu_section_02')?></p>
			</a>
		
			<a href="#explora" class="box-menu">
				<p><?=$this->lang->line('menu_section_03')?></p>
			</a>
		
			<a href="#contacto" class="box-menu">
				<p><?=$this->lang->line('menu_section_04')?></p>
			</a>
	</div>
</section>

<section class="sec-propuesta" style="background-image:url('<?=base_url()?>asset/img/back-cat.png');">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<h3><?=$this->lang->line('diagnostico_description')?>
				</h3>
				<hr>
			</div>
			<div class="col-12 col-md-2"></div>
			<div class="col-12 col-md-5">
			</div>
		</div>
	</div>
</section>

<section class="carrousel-categorias" id="soluciones">
	<div class="arrow-left-sect-prodct">
		<img src="<?=base_url()?>asset/img/arrow-left.png" class="img-fluid">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div id="destacados" class="owl-carousel owl-theme">
				  <div class="item">
				  	<div class="row w-100 m-0 align-items-stretch h-100">
				  		<div class="col-12 col-md-6 d-flex align-items-start flex-column justify-content-center">
				  			<h3><?=$this->lang->line('diagnostico_product_01_title')?></h3>
				  			<p><?=$this->lang->line('diagnostico_product_01_description')?></p>
				  			<a href="<?=$this->lang->line('diagnostico_product_01_link')?>" target="_blank"><?=$this->lang->line('lbl_ver_mas')?></a>
				  			<img src="<?=base_url()?>asset/img/arrow-back-prod.png" class="img-fluid arrow-absolute-product">
				  		</div>
				  		<div class="col-12 col-md-6">
				  			<img src="<?=base_url()?>asset/img/img-categoria-01-v2.png" class="img-fluid">
				  		</div>
				  	</div>
				  </div>
				  <div class="item">
				  	<div class="row w-100 m-0 align-items-stretch h-100">
				  		<div class="col-12 col-md-6 d-flex align-items-start flex-column justify-content-center">
				  			<h3><?=$this->lang->line('diagnostico_product_02_title')?></h3>
				  			<p><?=$this->lang->line('diagnostico_product_02_description')?></p>
				  			<a href="<?=$this->lang->line('diagnostico_product_02_link')?>" target="_blank"><?=$this->lang->line('lbl_ver_mas')?></a>
				  			<img src="<?=base_url()?>asset/img/arrow-back-prod.png" class="img-fluid arrow-absolute-product">
				  		</div>
				  		<div class="col-12 col-md-6">
				  			<?php if($shortname != 'pt'): ?>
				  				<img src="<?=base_url()?>asset/img/new-image-06.png" class="img-fluid">
				  			<?php else: ?>
				  				<img src="<?=base_url()?>asset/img/vital-sign-v2.png" class="img-fluid">
				  			<?php endif; ?>
				  		</div>
				  	</div>
				  </div>
				  <?php if($shortname != 'pt'): ?>
					  <div class="item">
					  	<div class="row w-100 m-0 align-items-stretch h-100">
					  		<div class="col-12 col-md-6 d-flex align-items-start flex-column justify-content-center">
					  			<h3><?=$this->lang->line('diagnostico_product_03_title')?></h3>
					  			<p><?=$this->lang->line('diagnostico_product_03_description')?></p>
					  			<a href="<?=$this->lang->line('diagnostico_product_03_link')?>" target="_blank"><?=$this->lang->line('lbl_ver_mas')?></a>
					  			<img src="<?=base_url()?>asset/img/arrow-back-prod.png" class="img-fluid arrow-absolute-product">
					  		</div>
					  		<div class="col-12 col-md-6">
					  			<img src="<?=base_url()?>asset/img/new-image-07.png" class="img-fluid">
					  		</div>
					  	</div>
					  </div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-12 text-center todos-link">
				<a href="<?=$this->lang->line('diagnostico_lbl_ver_todo')?>" target="_blank"><?=$this->lang->line('lbl_ver_todo')?></a>
			</div>
		</div>
	</div>
</section>

<section class="novedades" id="novedad">
	<div class="w-100" style="padding:0px 10vw;">
		<div class="row">
			<div class="col-12 text-center">
				<h3><?=$this->lang->line('title_innovacion')?></h3>
				<p><?=$this->lang->line('description_innovacion')?></p>
			</div>
			<div id="novedades" class="owl-carousel owl-theme">
				<?php if($shortname != 'pt'): ?>
				  <div class="item text-center">
				  	<a href="<?=$this->lang->line('diagnostico_innovacion_link_01')?>" target="_blank" class="contante-novedad">
				  		<div class="image-alternative cust-image-alternative" style="background-image:url('<?=base_url()?>asset/img/<?=$this->lang->line('diagnostico_innovacion_img_01')?>');"></div>
				  		<div class="min-h-nov">
					  		<h3><?=$this->lang->line('diagnostico_innovacion_title_01')?></h3>
					  		<hr>
					  		<p><?=$this->lang->line('diagnostico_innovacion_description_01')?></p>
				  		</div>
				  	</a>
				  	<a href="<?=$this->lang->line('diagnostico_innovacion_link_01')?>" target="_blank" class="cta-nov"><?=$this->lang->line('lbl_ver_mas')?></a>
				  </div>
				<?php endif; ?>
			  <div class="item text-center">
			  	<a href="<?=$this->lang->line('diagnostico_innovacion_link_02')?>" target="_blank" class="contante-novedad">
			  		<div class="image-alternative cust-image-alternative" style="background-image:url('<?=base_url()?>asset/img/<?=$this->lang->line('diagnostico_innovacion_img_02')?>');"></div>
			  		<div class="min-h-nov">
				  		<h3><?=$this->lang->line('diagnostico_innovacion_title_02')?></h3>
				  		<hr>
				  		<p><?=$this->lang->line('diagnostico_innovacion_description_02')?></p>
			  		</div>
			  	</a>
			  	<a href="<?=$this->lang->line('diagnostico_innovacion_link_02')?>" target="_blank" class="cta-nov"><?=$this->lang->line('lbl_ver_mas')?></a>
			  </div>
			  <div class="item text-center">
			  	<a href="<?=$this->lang->line('diagnostico_innovacion_link_03')?>" target="_blank" class="contante-novedad">
			  		<div class="image-alternative cust-image-alternative" style="background-image:url('<?=base_url()?>asset/img/nov-img-03.png');"></div>
			  		<div class="min-h-nov">
				  		<h3><?=$this->lang->line('diagnostico_innovacion_title_03')?></h3>
				  		<hr>
				  		<p><?=$this->lang->line('diagnostico_innovacion_description_03')?></p>
			  		</div>
			  	</a>
			  	<a href="<?=$this->lang->line('diagnostico_innovacion_link_03')?>" target="_blank" class="cta-nov"><?=$this->lang->line('lbl_ver_mas')?></a>
			  </div>
			</div>
		</div>
	</div>
</section>

<section class="explora" id="explora">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h3><?=$this->lang->line('title_solu_int')?></h3>
				<p><?=$this->lang->line('description_solu_int')?></p>
			</div>
		</div>
	</div>
</section>
<section class="responsiveboxmenu d-block d-sm-none">
	<div class="container">
		<div class="row">
			<div id="responsiveMenu" class="owl-carousel owl-theme">
			  <div class="item text-center">
			  	<a href="<?=$this->lang->line('url_01')?>" target="_top" class="swiper-slide hover-01-item" style="opacity:.5;pointer-events:none;">
  	      	<div class="back-menu-item" style="background-image:url('<?=base_url()?>asset/img/cat--07.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_01_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_01_description')?></p>
  	      </a>
			  </div>
			  <div class="item text-center">
  	      <a href="<?=$this->lang->line('url_02')?>" target="_top" class="swiper-slide hover-02-item">
  	      	<div class="back-menu-item" style="background: url('<?=base_url()?>asset/img/cat--08.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_02_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_02_description')?></p>
  	      </a>
			  </div>
			  <div class="item text-center">
  	      <a href="<?=$this->lang->line('url_03')?>" target="_top" class="swiper-slide hover-03-item" >
  	      	<div class="back-menu-item" style="background-image:url('<?=base_url()?>asset/img/cat--09.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_03_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_03_description')?></p>
  	      </a>
			  </div>
			  <div class="item text-center">
  	      <a href="<?=$this->lang->line('url_04')?>" target="_top" class="swiper-slide hover-04-item">
  	      	<div class="back-menu-item" style="background-image:url('<?=base_url()?>asset/img/cat--10.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_04_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_04_description')?></p>
  	      </a>
			  </div>
			</div>
		</div>
	</div>
</section>
<div class="menu-slider d-none d-sm-block">
  <div class="swiper-container swiper">
    <div class="swiper-wrapper">
      <a href="<?=$this->lang->line('url_01')?>" target="_top" class="swiper-slide hover-01-item" style="pointer-events:none;opacity:.5;">
      	<div class="video-01">
	      	<video id="video01" poster="<?=base_url()?>asset/img/cat--07.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_FLC Video_Loop_1.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=$this->lang->line('url_02')?>" target="_top" class="swiper-slide hover-02-item">
      	<div class="video-02">
	      	<video id="video02" poster="<?=base_url()?>asset/img/cat--08.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_GSS Video_Loop_1.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=$this->lang->line('url_03')?>" target="_top" class="swiper-slide hover-03-item">
      	<div class="video-03">
	      	<video id="video03" poster="<?=base_url()?>asset/img/cat--09.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_Connecta Video_Loop_1.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=$this->lang->line('url_04')?>" target="_top" class="swiper-slide hover-04-item">
      	<div class="video-04">
	      	<video id="video04" poster="<?=base_url()?>asset/img/cat--10.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_PSS Video_Loop.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
    </div>
  </div>
  <div class="swiper-container-dots swiper" style="pointer-events: none;">
    <div class="swiper-wrapper">
      <div class="dots swiper-slide nohover" style="color:#fff;pointer-events:none;">
      	<div class="content-ab item-01-p active-item-dot">
	      	<div class="circle-line">
	      		<i style="opacity:.5;"></i>
	      	</div>
	      	<p style="opacity:.5;"><?=$this->lang->line('menu_01_title')?></p>
	      	<p style="opacity:.5;" class="description-card"><?=$this->lang->line('menu_01_description')?></p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-02-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p><?=$this->lang->line('menu_02_title')?></p>
	      	<p class="description-card"><?=$this->lang->line('menu_02_description')?></p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-03-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p><?=$this->lang->line('menu_03_title')?></p>
	      	<p class="description-card"><?=$this->lang->line('menu_03_description')?></p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-04-p">
	      	<div class="circle-line lstch">
	      		<i></i>
	      	</div>
	      	<p><?=$this->lang->line('menu_04_title')?></p>
	      	<p class="description-card"><?=$this->lang->line('menu_04_description')?></p>
      	</div>
      </div>
    </div>
  </div>
</div>