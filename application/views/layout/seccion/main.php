<section class="back-home d-flex align-items-center" style="background:url('<?=base_url()?>asset/img/back-seccion.jpg');">
	<video id="videoheader" src="<?=base_url()?>asset/video/FLC2.mp4" loop muted autoplay>
	</video>
	<div class="absolute-icon">
		<img src="<?=base_url()?>asset/img/play-icon.png" class="img-fluid play-home">
	</div>
	<div class="filter-01"></div>
	<div class="filter-02"></div>
	<div class="container" style="position:relative;">
		<div class="row">
			<div class="col-12 col-md-4">
				<h3>FLC</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			</div>
		</div>
	</div>
	<div class="modal-video">
		<div class="container" style="position:relative;">
			<div class="row">	
				<div class="col-12 col-md-2"></div>
				<div class="col-12 col-md-8">
						<video id="videoId" src="https://hillrom.lat/content/dam/hillrom-aem/latam/en/marketing/gss/2022- GSS- Video_Español_High 02_08.mp4" controls loop muted autoplay></video>
						<div class="close-modal">X</div>
				</div>
				<div class="col-12 col-md-2"></div>
		    
	    </div>
    </div>
	</div>
</section>

<section class="secciones menu">
	<div class="row m-0 justify-content-center">
		<div class="col-12 col-md-2">
			<a href="#" class="box-menu">
				<p>INTRO GBU</p>
			</a>
		</div>
		<div class="col-12 col-md-2">
			<a href="#categorias" class="box-menu">
				<p>CATEGORÍAS</p>
			</a>
		</div>
		<div class="col-12 col-md-2">
			<a href="#novedad" class="box-menu">
				<p>NOVEDADES</p>
			</a>
		</div>
		<div class="col-12 col-md-2">
			<a href="#explora" class="box-menu">
				<p>EXPLORA HILLROM</p>
			</a>
		</div>
		<div class="col-12 col-md-2">
			<a href="#contacto" class="box-menu">
				<p>CONTACTO</p>
			</a>
		</div>
	</div>
</section>

<section class="sec-propuesta" style="background:url('<?=base_url()?>asset/img/back-cat.png');">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-5">
				<h3>Facilitar
				el diagnóstico
				y tratamiento
				temprano
				</h3>
				<img src="<?=base_url()?>asset/img/degrade-linea.png" class="img-fluid">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			</div>
			<div class="col-12 col-md-2"></div>
			<div class="col-12 col-md-5">
			</div>
		</div>
	</div>
</section>

<section class="carrousel-categorias" id="categorias">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div id="destacados" class="owl-carousel owl-theme">
				  <div class="item">
				  	<div class="row w-100 m-0 align-items-center">
				  		<div class="col-12 col-md-6">
				  			<h3>Examen Físico</h3>
				  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
				  			<a href="https://www.hillrom.lat/es/products/diagnostic-sets/" target="_blank">VER MÁS</a>
				  		</div>
				  		<div class="col-12 col-md-6">
				  			<img src="<?=base_url()?>asset/img/img-categoria-01.png" class="img-fluid">
				  		</div>
				  	</div>
				  </div>
				  <div class="item">
				  	<div class="row w-100 m-0 align-items-center">
				  		<div class="col-12 col-md-6">
				  			<h3>Cardiología</h3>
				  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
				  			<a href="https://www.hillrom.lat/es/products/spot-vital-signs-4400-device/" target="_blank">VER MÁS</a>
				  		</div>
				  		<div class="col-12 col-md-6">
				  			<img src="<?=base_url()?>asset/img/vital-sign.png" class="img-fluid">
				  		</div>
				  	</div>
				  </div>
				</div>
			</div>
			<div class="col-12 text-center todos-link">
				<a href="https://www.hillrom.lat/es/solutions/enable-earlier-diagnosis-and-treatment/" target="_blank">VER TODO</a>
			</div>
		</div>
	</div>
</section>

<section class="novedades" id="novedad">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h3>Novedades</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			</div>
			<div id="novedades" class="owl-carousel owl-theme">
			  <div class="item text-center">
			  	<div class="contante-novedad" style="background:url('<?=base_url()?>asset/img/nov-img-01.png');">
			  		<div class="filter-01-nov"></div>
			  		<div class="min-h-nov">
				  		<h3>Termometría</h3>
				  		<p>Eleva tu estándar desde el primer contacto</p>
			  		</div>
			  	</div>
			  	<a href="https://www.hillrom.lat/es/products/promociones/soluciones-de-termometria/" target="_blank" class="cta-nov">VER MÁS</a>
			  </div>
			  <div class="item text-center">
			  	<div class="contante-novedad" style="background:url('<?=base_url()?>asset/img/nov-img-02.png');">
			  		<div class="filter-02-nov"></div>
			  		<div class="min-h-nov">
				  		<h3>Green Series™ 777</h3>
				  		<p>El Sistema integrado de pared que lo tiene todo</p>
			  		</div>
			  	</div>
			  	<a href="https://www.hillrom.lat/es/products/promociones/wall-transformer-green-series-777/" target="_blank" class="cta-nov">VER MÁS</a>
			  </div>
			  <div class="item text-center">
			  	<div class="contante-novedad" style="background:url('<?=base_url()?>asset/img/nov-img-03.png');">
			  		<div class="min-h-nov">
				  		<h3>Spot™ Vision Screener</h3>
				  		<p>Mira otro<br>futuro</p>
			  		</div>
			  	</div>
			  	<a href="https://www.hillrom.lat/es/products/promociones/spot--vision-screener/" target="_blank" class="cta-nov">VER MÁS</a>
			  </div>
			</div>
		</div>
	</div>
</section>

<section class="explora" id="explora">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h3>Explora Hillrom</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			</div>
		</div>
	</div>
</section>
<div class="menu-slider" style="margin-top:0px;background: #1b2154;">
  <div class="swiper-container swiper">
    <div class="swiper-wrapper">
      <a href="<?=base_url()?>seccion/" class="swiper-slide hover-01-item">
      	<div class="video-01">
	      	<video id="video01" poster="<?=base_url()?>asset/img/cat--07.png" controls muted="muted">
	      		<source src="https://hillrom.lat//content/dam/hillrom-aem/latam/en/marketing/gss/Landing%20GSS%20Videos_Modulo%2001.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=base_url()?>seccion/" class="swiper-slide hover-02-item">
      	<div class="video-02">
	      	<video id="video02" poster="<?=base_url()?>asset/img/cat--08.png" controls muted="muted">
	      		<source src="https://hillrom.lat//content/dam/hillrom-aem/latam/en/marketing/gss/Landing%20GSS%20Videos_Modulo%2001.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=base_url()?>seccion/" class="swiper-slide hover-03-item">
      	<div class="video-03">
	      	<video id="video03" poster="<?=base_url()?>asset/img/cat--09.png" controls muted="muted">
	      		<source src="https://hillrom.lat//content/dam/hillrom-aem/latam/en/marketing/gss/Landing%20GSS%20Videos_Modulo%2001.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=base_url()?>seccion/" class="swiper-slide hover-04-item">
      	<div class="video-04">
	      	<video id="video04" poster="<?=base_url()?>asset/img/cat--10.png" controls muted="muted">
	      		<source src="https://hillrom.lat//content/dam/hillrom-aem/latam/en/marketing/gss/Landing%20GSS%20Videos_Modulo%2001.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
    </div>
  </div>
  <div class="swiper-container-dots swiper" style="pointer-events: none;">
    <div class="swiper-wrapper">
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-01-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p>LOREM</p>
	      	<p class="description-card">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-02-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p>LOREM</p>
	      	<p class="description-card">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-03-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p>LOREM</p>
	      	<p class="description-card">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-04-p">
	      	<div class="circle-line lstch">
	      		<i></i>
	      	</div>
	      	<p>LOREM</p>
	      	<p class="description-card">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
      	</div>
      </div>
    </div>
  </div>
</div>