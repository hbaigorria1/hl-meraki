<section class="back-home d-flex align-items-center" style="min-height:100vh;">
	<video id="videoheader" src="<?=base_url()?>asset/video/21-0644-ALL-A Meraki Video Editing_Loop_1.mp4" loop muted autoplay>
	</video>
	<div class="absolute-icon">
		<img src="<?=base_url()?>asset/img/play-icon.png" class="img-fluid play-home">
	</div>
	<div class="filter-02"></div>
	<div class="filter-01"></div>
	<div class="container postition-cont-home">
		<div class="row">
			<div class="col-12 col-md-2"></div>
			<div class="col-12 col-md-8 text-center">
				<h3><?=$this->lang->line('title')?></h3>
				<p><?=$this->lang->line('description')?></p>
			</div>
			<div class="col-12 col-md-2"></div>
		</div>
	</div>
	<div class="modal-video">
		<div class="container" style="position:relative;">
			<div class="row">	
				<div class="col-12 col-md-12">
						<video id="videoId" src="<?=base_url()?>asset/video/<?=$this->lang->line('video_01')?>" controls loop muted></video>
						<div class="close-modal">X</div>
				</div>
		    
	    </div>
    </div>
	</div>
	<img src="<?=base_url()?>asset/img/triangle-left.png" class="img-fluid absolut-image-home">
</section>
<section class="responsiveboxmenu d-block d-sm-none">

	<div class="container">
		<div class="row">
			<div id="responsiveMenu" class="owl-carousel owl-theme">
			  <div class="item text-center">
			  	<a href="<?=$this->lang->line('url_01')?>" class="swiper-slide hover-01-item" target="_top">
  	      	<div class="back-menu-item" style="background:url('<?=base_url()?>asset/img/cat--07.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_01_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_01_description')?></p>
  	      </a>
			  </div>
			  <div class="item text-center">
  	      <a href="<?=$this->lang->line('url_02')?>" class="swiper-slide hover-02-item" target="_top">
  	      	<div class="back-menu-item" style="background: url('<?=base_url()?>asset/img/cat--08.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_02_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_02_description')?></p>
  	      </a>
			  </div>
			  <div class="item text-center">
  	      <a href="<?=$this->lang->line('url_03')?>" class="swiper-slide hover-03-item" target="_top">
  	      	<div class="back-menu-item" style="background:url('<?=base_url()?>asset/img/cat--09.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_03_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_03_description')?></p>
  	      </a>
			  </div>
			  <div class="item text-center">
  	      <a href="<?=$this->lang->line('url_04')?>" class="swiper-slide hover-04-item" target="_top">
  	      	<div class="back-menu-item" style="background:url('<?=base_url()?>asset/img/cat--10.png');">
  	      	</div>
  	      	<p class="d-block d-sm-none"><?=$this->lang->line('menu_04_title')?></p>
  	      	<p class="description-card d-block d-sm-none" style="opacity:1;transform:none;position:relative;"><?=$this->lang->line('menu_04_description')?></p>
  	      </a>
			  </div>
			</div>
		</div>
	</div>
</section>
<div class="menu-slider d-none d-sm-block">
	<div class="filter-top"></div>
  <div class="swiper-container swiper">
    <div class="swiper-wrapper">
      <a href="<?=$this->lang->line('url_01')?>" class="swiper-slide hover-01-item" target="_top">
      	<div class="video-01">
	      	<video id="video01" poster="<?=base_url()?>asset/img/cat--07.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_FLC Video_Loop_1.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=$this->lang->line('url_02')?>" class="swiper-slide hover-02-item" target="_top">
      	<div class="video-02">
	      	<video id="video02" poster="<?=base_url()?>asset/img/cat--08.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_GSS Video_Loop_1.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=$this->lang->line('url_03')?>" class="swiper-slide hover-03-item" target="_top">
      	<div class="video-03">
	      	<video id="video03" poster="<?=base_url()?>asset/img/cat--09.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_Connecta Video_Loop_1.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
      <a href="<?=$this->lang->line('url_04')?>" class="swiper-slide hover-04-item" target="_top">
      	<div class="video-04">
	      	<video id="video04" poster="<?=base_url()?>asset/img/cat--10.png" controls muted="muted">
	      		<source src="<?=base_url()?>asset/video/22-HIL-0093 - Meraki GBU Videos_PSS Video_Loop.mp4" type="video/mp4">
	      	</video>
      	</div>
      </a>
    </div>
  </div>
  <div class="swiper-container-dots swiper" style="pointer-events: none;">
    <div class="swiper-wrapper">
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-01-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p><?=$this->lang->line('menu_01_title')?></p>
	      	<p class="description-card"><?=$this->lang->line('menu_01_description')?></p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-02-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p><?=$this->lang->line('menu_02_title')?></p>
	      	<p class="description-card"><?=$this->lang->line('menu_02_description')?></p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-03-p">
	      	<div class="circle-line">
	      		<i></i>
	      	</div>
	      	<p><?=$this->lang->line('menu_03_title')?></p>
	      	<p class="description-card"><?=$this->lang->line('menu_03_description')?></p>
      	</div>
      </div>
      <div class="dots swiper-slide nohover" style="color:#fff">
      	<div class="content-ab item-04-p">
	      	<div class="circle-line lstch">
	      		<i></i>
	      	</div>
	      	<p><?=$this->lang->line('menu_04_title')?></p>
	      	<p class="description-card"><?=$this->lang->line('menu_04_description')?></p>
      	</div>
      </div>
    </div>
  </div>
</div>
<?php if(false): ?>
<section class="secciones">
	<div class="row m-0">
		<div class="col-12 col-md-3">
			<a href="<?=base_url().$this->config->item('language_abbr')?>/seccion/" class="box-menu">
				<div class="back-menu" style="background: url('<?=base_url()?>asset/img/cat--07.png');"></div>
				<p>LOREM IPSUM</p>
			</a>
		</div>
		<div class="col-12 col-md-3">
			<a href="<?=base_url().$this->config->item('language_abbr')?>/seccion/" class="box-menu">
				<div class="back-menu" style="background: url('<?=base_url()?>asset/img/cat--08.png');"></div>
				<p>LOREM IPSUM</p>
			</a>
		</div>
		<div class="col-12 col-md-3">
			<a href="<?=base_url().$this->config->item('language_abbr')?>/seccion/" class="box-menu">
				<div class="back-menu" style="background: url('<?=base_url()?>asset/img/cat--09.png');"></div>
				<p>LOREM IPSUM</p>
			</a>
		</div>
		<div class="col-12 col-md-3">
			<a href="<?=base_url().$this->config->item('language_abbr')?>/seccion/" class="box-menu">
				<div class="back-menu" style="background: url('<?=base_url()?>asset/img/cat--10.png');"></div>
				<p>LOREM IPSUM</p>
			</a>
		</div>
	</div>
</section>
<?php endif; ?>
<section class="content-titles">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2><?=$this->lang->line('title_01_principal')?></h2>
				<h3><?=$this->lang->line('title_02')?></h3>
				<p><?=$this->lang->line('description_02')?></p>
				<a href="#contacto"><img src="<?=base_url()?>asset/img/icon-arrow-down.png" class="img-fluid"></a>
			</div>
		</div>
	</div>
	<img src="<?=base_url()?>asset/img/right-image-triangle.png" class="img-fluid img-foot-sec-home">
</section>

<section class="section-new" style="background-image:url('<?=base_url()?>asset/img/image-home-new.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h4>+100 <small><?=$this->lang->line('anios_01')?></small></h4>
					<p><?=$this->lang->line('anios_02')?></p>
				</div>
				<div class="col-12 col-md-2"></div>
				<div class="col-12 col-md-8">
					<hr>
					<div class="flex-div w-100 d-flex align-items-center justify-content-center">
						<?=$this->lang->line('anios_03')?>
					</div>
				</div>
				<div class="col-12 col-md-2"></div>
			</div>
		</div>
</section>
<section class="section-new-bot" style="background-image:url('<?=base_url()?>asset/img/arrow-beetween.png');">
	<div class="container">
		<div class="row align-items-stretch justify-content-center">
			<div class="col-12 col-md-3 d-flex flex-column">
				<h5>+350 <small><?=$this->lang->line('new_info_home_box_01')?></small></h5>
				<p><?=$this->lang->line('new_info_home_box_01_desc')?></p>
				<hr style="background:#00BDAD;">
			</div>
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-3 d-flex flex-column">
				<h5>+200</h5>
				<p><?=$this->lang->line('new_info_home_box_02_desc')?></p>
				<hr style="background:#009FE3;">
			</div>
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-3 d-flex flex-column">
				<h5>+50 <small>MIL</small></h5>
				<p><?=$this->lang->line('new_info_home_box_03_desc')?></p>
				<hr style="background:#62B86C;">
			</div>
		</div>
	</div>
</section>