<div class="w-100" style="height: 100%;position: absolute;backface-visibility: hidden;background: #000;display: flex;align-items: center;">
	<div class="container">
		<div class="col-md-12 text-center">
			<p style="color: #fff;margin: 0;font-size: 26px;font-family: Montserrat-Light;">¡Gracias por contactarte con nosotros!<br>Un asesor se comunicará a la brevedad.</p>
		</div>
	</div>
</div>