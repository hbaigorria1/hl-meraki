<!DOCTYPE html>
<html>
   <head>
   	  <meta charset="utf-8" />
      <title><?= $title ?></title>
	  <meta name="description" content="">
	  <meta name="keywords" content="">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
	  <meta property="og:title" content="<?= $title ?>" />
	  <meta property="og:type" content="<?= $ogType ?>" />
	  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
	  <meta property="og:image" content="" />
	  <meta property="og:description" content="" />
	  
	  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>asset/img/favicon.png?">

	  <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css"/>

	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'>
	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css'>
	  
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css'>
	  <link rel="preconnect" href="https://fonts.gstatic.com">
	  <link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">

	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/custom.css?version=<?=time()?>" type="text/css" media="all" />

      <script src='https://cdnjs.cloudflare.com/ajax/libs/bignumber.js/9.0.0/bignumber.min.js'></script>


	  <script>var base_url = "<?php echo base_url() ?>";</script>
	  
	  <?= $_styles ?>

	  <!-- Google Tag Manager -->
	  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	  })(window,document,'script','dataLayer','GTM-N5S4DXZM');</script>
	  <!-- End Google Tag Manager -->

   </head>
   <body>

   	<!-- Google Tag Manager (noscript) -->
   	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5S4DXZM"
   	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
   	<!-- End Google Tag Manager (noscript) -->
   	
	  <?= $header ?>        
	  <?= $content ?>
    <?= $footer ?>

    <div class="btn-home">
    	<a href="<?=$this->lang->line('home')?>" target="_top">
    		<i class="fas fa-home"></i>
    	</a>
    </div>

    <div class="btn-hl">
    	<a href="<?=$this->lang->line('hl_link')?>" target="_blank">
    		<img src="<?=base_url()?>asset/img/icon-baxter.png" class="img-fluid">
    	</a>
    </div>
    
	  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/pagePiling.js/1.5.6/jquery.pagepiling.min.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js'></script>
	  <script src='https://use.fontawesome.com/826a7e3dce.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js'></script>
      
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	
	  <script defer type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.hoverplay.min.js"></script>
	  <script defer type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js?version=<?=time()?>"></script>
	  <!-- Swiper JS -->
	  <script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>

	  <?= $_scripts ?>


	  <script type="text/javascript">
	  	// Script dentro del iframe
	  	window.addEventListener('scroll', () => {
	  	    const scrollPosition = window.scrollY; // o document.documentElement.scrollTop
	  	    console.log('Posición actual del scroll:', scrollPosition);

	  	    // Opcional: Enviar esta información al documento principal
	  	    window.parent.postMessage({
	  	        type: 'scrollPosition',
	  	        scroll: scrollPosition
	  	    }, '*'); // Deberías restringir el origen en un caso real
	  	});
	  </script>

	  <script type="text/javascript">
	  	$(".contact-form").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.gif').show('slow');
	  		        $('.message').hide("slow");
	  		    },
	  		    success: function(response){
  		          	window.location.replace("<?php echo base_url() ?>home/gracias/");
	  		    }
	  		});
	  	});
	  </script>

	  <script type="text/javascript">
	  	$('#destacados').owlCarousel({
	  	    loop: true,
	  	    margin: 10,
	  	    dots: true,
	  	    nav: true,
	  	    autoplay:true,
    		 autoplayTimeout:3000,
    		 autoplayHoverPause:true,
	  	    responsive:{
	  	        0:{
	  	            items:1,
	  	        },
	  	        600:{
	  	            items:1,
	  	        },
	  	        1000:{
	  	            items:1,
	  	        }
	  	    }
	  	});
	  	$('#novedades').owlCarousel({
	  	    loop: false,
	  	    margin: 10,
	  	    dots: false,
	  	    nav: true,
	  	    responsive:{
	  	        0:{
	  	            items:1,
	  	        },
	  	        600:{
	  	            items:1,
	  	        },
	  	        1000:{
	  	            items:4,
	  	        }
	  	    }
	  	});

	  	$('#responsiveMenu').owlCarousel({
	  	    loop: false,
	  	    margin: 10,
	  	    dots: true,
	  	    nav: false,
	  	    responsive:{
	  	        0:{
	  	            items:1,
	  	        },
	  	        600:{
	  	            items:1,
	  	        },
	  	        1000:{
	  	            items:1,
	  	        }
	  	    }
	  	});
	  </script>

	  <!-- Initialize Swiper -->
	  <script>
	    var swiper = new Swiper('.swiper-container', {
	      slidesPerView: 4,
	      spaceBetween: 10,
	      slidesPerGroup: 2,
	      pagination: {
	        el: '.swiper-pagination',
	        clickable: true,
	      },
	      navigation: {
	        nextEl: '.swiper-button-next',
	        prevEl: '.swiper-button-prev',
	      },

	      breakpoints: {
	          // when window width is >= 320px
	          320: {
	            slidesPerView: 1,
	            spaceBetween: 20
	          },
	          // when window width is >= 480px
	          480: {
	            slidesPerView: 4,
	            spaceBetween: 10
	          },
	          // when window width is >= 640px
	          1200: {
	            slidesPerView: 4,
	            spaceBetween: 10
	          }
	      }
	    });

	    var swiper2 = new Swiper('.swiper-container-dots', {
	      slidesPerView: 4,
	      spaceBetween: 10,
	      slidesPerGroup: 2,
	      pagination: {
	        el: '.swiper-pagination2',
	        clickable: true,
	      },
	      navigation: {
	        nextEl: '.swiper-button-next',
	        prevEl: '.swiper-button-prev',
	      },
	    });

	    $( ".play-home" ).click(function() {
	      $( ".modal-video" ).addClass('transition-modal');
	      $('#videoId').get(0).currentTime = 0;
	      $('#videoheader').trigger('pause');
	      $('#videoheader').addClass('blurvideo');
	      $('#videoId').get(0).play(); // Reproducir el video
	    });
	    $( ".close-modal" ).click(function() {
	      $( ".modal-video" ).removeClass('transition-modal');
	      $('#videoId').get(0).currentTime = 0;
	      var video = document.getElementById("videoId");
	      video.muted = video.muted;
	      $('#videoheader').removeClass('blurvideo');
	      $('#videoheader').trigger('play');
	      $('#videoId').get(0).pause();
	    });


	    $(".hover-01-item").hover(
	    	function() {
			    $( ".item-01-p" ).addClass( 'active-item-dot' );
			  }, function() {
			    $( ".item-01-p" ).removeClass( 'active-item-dot' );
			  }
	    );

	    $(".hover-02-item").hover(
	    	function() {
			    $( ".item-02-p" ).addClass( 'active-item-dot' );
			  }, function() {
			    $( ".item-02-p" ).removeClass( 'active-item-dot' );
			  }
	    );

	    $(".hover-03-item").hover(
	    	function() {
			    $( ".item-03-p" ).addClass( 'active-item-dot' );
			  }, function() {
			    $( ".item-03-p" ).removeClass( 'active-item-dot' );
			  }
	    );


	    $(".hover-04-item").hover(
	    	function() {
			    $( ".item-04-p" ).addClass( 'active-item-dot' );
			  }, function() {
			    $( ".item-04-p" ).removeClass( 'active-item-dot' );
			  }
	    );


	    $(window).scroll(function() {    
	        var scroll = $(window).scrollTop();

	         //>=, not <=
	        if (scroll > 700) {
	            //clearHeader, not clearheader - caps H
	            $(".menu").addClass("absolute-menu");
	        } else {
	            $(".menu").removeClass("absolute-menu");
	        }
	    }); //missing );


	    var sections = $('section')
	    	  	    , nav = $('.content-menu')
	    	  	    , nav_height = nav.outerHeight();

	    	  	  $(window).on('scroll', function () {
	    	  	    var cur_pos = $(this).scrollTop();
	    	  	    
	    	  	    sections.each(function() {
	    	  	      var top = $(this).offset().top - nav_height,
	    	  	          bottom = top + $(this).outerHeight();
	    	  	      
	    	  	      if (cur_pos >= top && cur_pos <= bottom) {
	    	  	        nav.find('a').removeClass('active');
	    	  	        sections.removeClass('active');
	    	  	        
	    	  	        $(this).addClass('active');
	    	  	        nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
	    	  	      }
	    	  	    });
	    	  	  });

	    	  	  nav.find('a').on('click', function () {
	    	  	    var $el = $(this)
	    	  	      , id = $el.attr('href');
	    	  	    
	    	  	    $('html, body').animate({
	    	  	      scrollTop: $(id).offset().top
	    	  	    }, 500);
	    	  	    
	    	  	    return false;
	    	  	  });
	    
	  </script>

   </body>
</html>