<?php

class page_model extends CI_Model
{


		public function __construct()
        {
			parent::__construct();
			// Your own constructor code
        }
        
        public $title;
        public $content;
        public $date;


		public function get_catalogos()
		{
			$result = $this->db->get('catalogos');
			return $result->result();
		}

		public function get_paises()
		{
			$this->db->order_by('country', 'asc');
			$result = $this->db->get('countries');
			return $result->result();
		}

		public function get_catalogo_slug($slug)
		{	
			$this->db->where('slug', $slug);
			$result = $this->db->get('catalogos');
			return $result->result();
		}

		public function get_categorias($slug)
		{
			$this->db->select('categorias.*');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->where('catalogos.slug', $slug);
			$this->db->join('catalogos', 'catalogos.id = categorias.id_catalogo');
			$result = $this->db->get('categorias');
			return $result->result();
		}

		public function get_sub_categeoria($id_categoria)
		{
			$this->db->select('subcategorias.*');
			$this->db->select('categorias.nombre as categoria');
			$this->db->select('categorias.slug as slug_categoria');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->join('categorias', 'categorias.id = subcategorias.id_categoria');
			$this->db->join('catalogos', 'catalogos.id = categorias.id_catalogo');
			$this->db->where('subcategorias.id_categoria', $id_categoria);
			$result = $this->db->get('subcategorias');
			return $result->result();
		}

		public function get_act_categoria($slug_categoria , $slug_subcategoria)
		{
			if(!empty($slug_subcategoria)):
				$this->db->select('subcategorias.*');
				$this->db->select('categorias.nombre as categoria');
				$this->db->where('categorias.slug', $slug_categoria);
				$this->db->where('subcategorias.slug', $slug_subcategoria);
				$this->db->join('categorias', 'categorias.id = subcategorias.id_categoria');
				$result = $this->db->get('subcategorias');
				return $result->result();
			else:
				$this->db->where('categorias.slug', $slug_categoria);
				$result = $this->db->get('categorias');
				return $result->result();
			endif;
		}

		public function get_productos($slug_categoria , $slug_subcategoria)
		{
			if(!empty($slug_subcategoria)):
				$this->db->select('productos.*');
				$this->db->where('categorias.slug', $slug_categoria);
				$this->db->where('subcategorias.slug', $slug_subcategoria);
				$this->db->join('categorias', 'categorias.id = productos.id_categoria');
				$this->db->join('subcategorias', 'subcategorias.id = productos.id_subcategoria');
				$result = $this->db->get('productos');
				return $result->result();
			else:
				$this->db->select('productos.*');
				$this->db->where('categorias.slug', $slug_categoria);
				$this->db->join('categorias', 'categorias.id = productos.id_categoria');
				$result = $this->db->get('productos');
				return $result->result();
			endif;
		}

		public function get_producto($slug_categoria , $slug_subcategoria, $slug_producto)
		{
			$this->db->select('productos.*');
			$this->db->where('categorias.slug', $slug_categoria);
			$this->db->where('subcategorias.slug', $slug_subcategoria);
			$this->db->where('productos.slug', $slug_producto);
			$this->db->join('categorias', 'categorias.id = productos.id_categoria');
			$this->db->join('subcategorias', 'subcategorias.id = productos.id_subcategoria');
			$result = $this->db->get('productos');
			return $result->result();
		}
		
}

?>