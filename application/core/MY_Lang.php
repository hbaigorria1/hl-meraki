<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Lang extends CI_Lang
{
    function __construct() {
        parent::__construct();

        global $URI, $CFG, $IN;

        require_once(BASEPATH .'database/DB.php');
        $db =& DB();
        $query = $db->query('SELECT * FROM countries');
        $result = $query->result();

        $ct = array();
        foreach ($result as $row) {
            $ct[$row->{'shortname'}] = $row->{'language'};
        }

        $config =& $CFG->config;

        $index_page    = $config['index_page'];
        $lang_ignore   = @$config['lang_ignore'];
        $default_abbr  = @$config['language_abbr'];    
        $lang_uri_abbr = $ct;

        $uri_abbr = $URI->segment(1);

        $ip = $this->get_ip_address();

        // Log para depuración
        log_message('debug', 'IP Address: ' . $ip);

        $uri_country = strtolower($this->get_country_code_from_ip($ip));
        
        // Log para depuración
        log_message('debug', 'Country from IP: ' . $uri_country);

        // Verifica si el país obtenido de la IP coincide con algún shortname de la base de datos
        if (array_key_exists($uri_country, $ct)) {
            $default_abbr = $uri_country;
        } else {
            $default_abbr = "ar";
        }

        // Log para depuración
        log_message('debug', 'Default Abbr: ' . $default_abbr);

        $IN->set_cookie('user_ip', $ip, $config['sess_expiration']);

        $URI->uri_string = preg_replace("|^\/?|", '/', $URI->uri_string);

        if ($lang_ignore) {
            if (isset($lang_uri_abbr[$uri_abbr])) {
                $IN->set_cookie('user_lang', $uri_abbr, $config['sess_expiration']);
            } else {
                $lang_abbr = $IN->cookie($config['cookie_prefix'].'user_lang');
            }

            if (strlen($uri_abbr) == 2) {
                $index_page .= empty($index_page) ? '' : '/';
                $URI->uri_string = preg_replace("|^\/?$uri_abbr\/?|", '', $URI->uri_string);
                $url = $_SERVER['QUERY_STRING'];
                $url = "/?".$url;

                header('Location: '.$config['base_url'].$index_page.$URI->uri_string.$url);
                exit;
            }
        } else {
            $lang_abbr = $uri_abbr;
        }

        if (isset($lang_uri_abbr[$lang_abbr])) {
            $URI->segment(array_shift($URI->segments));
            $URI->uri_string = preg_replace("|^\/?$lang_abbr|", '', $URI->uri_string);

            $config['language'] = $lang_uri_abbr[$lang_abbr];
            $config['language_abbr'] = $lang_abbr;

            foreach ($result as $row) {
                if ($row->{'shortname'} == $lang_abbr) {
                    $config['country'] = $row->{'country'};
                    $config['country_id'] = $row->{'id'};
                }
            }

            $lang_ignore = false;

            if (!$lang_ignore) {
                $index_page .= empty($index_page) ? $lang_abbr : "/$lang_abbr";
                $config['index_page'] = $index_page;
            }

            $IN->set_cookie('user_lang', $lang_abbr, $config['sess_expiration']);
        } else {
            if (!$lang_ignore) {
                if (strlen($IN->cookie($config['cookie_prefix'].'user_lang')) == 2) {
                    $default_abbr = $IN->cookie($config['cookie_prefix'].'user_lang');
                } else {
                    $ip = $this->get_ip_address();
                    $uri_country = strtolower($this->get_country_code_from_ip($ip));

                    // Log para depuración
                    log_message('debug', 'Country from IP in else: ' . $uri_country);

                    if (array_key_exists($uri_country, $ct)) {
                        $default_abbr = $uri_country;
                    } else {
                        $default_abbr = "ar";
                    }
                }

                $index_page .= empty($index_page) ? $default_abbr : "/$default_abbr";

                if (strlen($lang_abbr) == 2) {
                    $URI->uri_string = preg_replace("|^\/?$lang_abbr|", '', $URI->uri_string);
                }

                $url = $_SERVER['QUERY_STRING'];
                $url = "/?".$url;

                if ($url == "/?") $url = "/";

                $IN->set_cookie('user_lang', $default_abbr, $config['sess_expiration']);

                header('Location: '.$config['base_url'].$index_page.$URI->uri_string.$url);
                exit;
            }

            $IN->set_cookie('user_lang', $default_abbr, $config['sess_expiration']);
        }
    }

    function get_ip_address() {
        $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR', 'SERVER_ADDR');
        foreach ($ip_keys as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);
                    if ($this->validate_ip($ip)) {
                        return $ip;
                    }
                }
            }
        }
        return false;
    }

    function validate_ip($ip) {
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false;
    }

    function get_country_code_from_ip($ip) {
        $url = "http://ipinfo.io/{$ip}/country";
        $ch = curl_init();

        // Establecer las opciones de cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FAILONERROR, true); // Hacer que cURL falle en errores HTTP

        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_error = curl_error($ch);

        curl_close($ch);

        // Log para depuración


        if ($httpcode != 200 || $response === FALSE) {
            log_message('error', 'Failed to fetch country code from IP: ' . $ip . ' with response: ' . $response);
            return null;
        }

        $response = trim($response);
        if (empty($response)) {
            log_message('error', 'Empty response for country code from IP: ' . $ip);
            return null;
        }

        return $response;
    }
}

/* translate helper */
function t($line) {
    global $LANG;
    return ($t = $LANG->line($line)) ? $t : $line;
}
?>
