<?php 

$lang['header'] = "Hillrom é parte da Baxter";
$lang['hl_link'] = "https://www.hillrom.lat/pt/";
$lang['home'] = "https://www.hillrom.lat/pt/juntos-pela-saude/";

$lang['url_01'] = "https://www.hillrom.lat/pt/juntos-pela-saude/diagnostico-primario/";
$lang['url_02'] = "https://www.hillrom.lat/pt/juntos-pela-saude/solucoes-cirurgicas/";
$lang['url_03'] = "https://www.hillrom.lat/pt/juntos-pela-saude/atendimento-conectado/";
$lang['url_04'] = "https://www.hillrom.lat/pt/juntos-pela-saude/atendimento-especializado/";

$lang['form_01'] = "https://pardot.hillrom.com/l/8232/2022-08-25/cfp124";
$lang['form_02'] = "https://pardot.hillrom.com/l/8232/2022-08-25/cfp15j";
$lang['form_03'] = "https://pardot.hillrom.com/l/8232/2022-08-25/cfp15q";
$lang['form_04'] = "https://pardot.hillrom.com/l/8232/2022-08-25/cfp15m";
$lang['form_05'] = "https://pardot.hillrom.com/l/8232/2022-08-25/cfp15t";

$lang['video_01'] = "21-0644-ALL-A Meraki Video Editing_Portugues_Final_High.mp4";
$lang['video_02'] = "22-HIL-0093 - Meraki GBU Videos_Connecta Video_Final Portugues.mp4";
$lang['video_03'] = "22-HIL-0093 - Meraki GBU Videos_FLC Video_Final Portugues.mp4";
$lang['video_04'] = "22-HIL-0093 - Meraki GBU Videos_PSS Video_Final Portugues_V02.mp4";
$lang['video_05'] = "22-HIL-0093 - Meraki GBU Videos_GSS Video_Final Portugues.mp4";

$lang['title'] = 'Salvar e prolongar vidas';
$lang['description'] = 'Essa é a nossa missão...<br>e a sua! Descubra como nossas soluções última geração podem ajudá-lo a fazer isso acontecer. Cada dia. Todos os dias.';

$lang['title_01_principal'] = 'Nós somos a nova Baxter.';
$lang['title_02'] = 'Somos o seu melhor aliado.';
$lang['description_02'] = "Com nossas soluções de ponta, você e seu hospital estarão<br>mais bem preparados para cuidar da saúde dos pacientes e responder<br>de forma mais proativa, segura e eficaz.";

$lang['anios_01'] = 'ANOS';
$lang['anios_02'] = 'de legado combinado entre nossas marcas,<br>oferecendo inovação médica com foco<br>no atendimento ao paciente.';
$lang['anios_03'] = '
<span>WELCH<br>ALLYN</span>
<span>MORTARA</span>
<span>HILLROM</span>
<span>TRUMPF<br>MEDICAL</span>
<span>ALLEN</span>';


$lang['menu_01_title'] = "DIAGNÓSTICO PRIMÁRIO";
$lang['menu_01_description'] = "Veja o que há de mais moderno em instrumentos de avaliação e você verá como possibilitar um diagnóstico mais precoce e preciso para o paciente.";

$lang['menu_02_title'] = "SOLUÇÕES CIRÚRGICAS";
$lang['menu_02_description'] = "Explore opções inovadoras para agilizar o fluxo de trabalho da sala de cirurgia e otimizar a segurança do paciente.";

$lang['menu_03_title'] = "ATENDIMENTO CONECTADO";
$lang['menu_03_description'] = "Experimente um novo nível de eficiência e integração na comunicação e colaboração da equipe de atendimento com nossa avançada plataforma digital Connecta.";

$lang['menu_04_title'] = "ATENDIMENTO ESPECIALIZADO";
$lang['menu_04_description'] = "Conheça nossas ferramentas especialmente desenvolvidas para acelerar a recuperação do paciente em qualquer ambiente hospitalar.";

$lang['title_contacto'] = "Entre em contato conosco";
$lang['description_contacto'] = "Para mais informações ou para falar diretamente com um representante de vendas sobre nossos produtos, soluções e serviços, preencha o formulário abaixo.";

$lang['menu_section_01'] = "SOLUÇÕES";
$lang['menu_section_02'] = "NOVIDADES";
$lang['menu_section_03'] = "EXPLORE";
$lang['menu_section_04'] = "CONTATO";

$lang['lbl_ver_mas'] = "VER MAIS";
$lang['lbl_ver_todo'] = "VER TUDO";

$lang['diagnostico_lbl_ver_todo'] = "https://www.hillrom.lat/pt/solutions/enable-earlier-diagnosis-and-treatment/";

$lang['diagnostico_title'] = "Diagnóstico precoce, tratamento oportuno";
$lang['diagnostico_description'] = "Nossos dispositivos inteligentes e instrumentos digitais reduzem os tempos de avaliação e aumentam sua precisão.";

$lang['diagnostico_product_01_title'] = "Exame Físico";
$lang['diagnostico_product_01_description'] = "Agora você pode realizar avaliações de deteção e monitoramento de visão com maior precisão e agilidade.";
$lang['diagnostico_product_01_link'] = "https://www.hillrom.lat/pt/products/diagnostic-sets/";

$lang['diagnostico_product_02_title'] = "Cardiologia";
$lang['diagnostico_product_02_description'] = "Expanda os recursos das avaliações de cardiologia com nossas soluções avançadas de diagnóstico.";
$lang['diagnostico_product_02_link'] = "https://www.hillrom.lat/pt/cardiologia-diagnostica/";

$lang['diagnostico_product_03_title'] = "Monitoreo";
$lang['diagnostico_product_03_description'] = "Conectados a tu propósito, nuestras soluciones de monitoreo permiten la medición de signos vitales de manera precisa y eficiente.";
$lang['diagnostico_product_03_link'] = "https://www.hillrom.lat/pt/solutions/enable-earlier-diagnosis-and-treatment/";


$lang['diagnostico_innovacion_title_01'] = "Termometria";
$lang['diagnostico_innovacion_description_01'] = "Eleve seu padrão desde o primeiro contato";
$lang['diagnostico_innovacion_link_01'] = "https://www.hillrom.lat/pt/products/promotions/soluciones-de-termometria/";
$lang['diagnostico_innovacion_img_01'] = "nov-img-01.png";

$lang['diagnostico_innovacion_title_02'] = "Green Series™ 777";
$lang['diagnostico_innovacion_description_02'] = "O sistema integrado de parede que tem de tudo";
$lang['diagnostico_innovacion_link_02'] = "https://www.hillrom.lat/pt/products/promotions/wall-transformer-green-series-777/";
$lang['diagnostico_innovacion_img_02'] = "nov-img-02.png";

$lang['diagnostico_innovacion_title_03'] = "Spot™ Vision Screener";
$lang['diagnostico_innovacion_description_03'] = "Olhe outro<br>futuro";
$lang['diagnostico_innovacion_link_03'] = "https://www.hillrom.lat/pt/products/promotions/spot--vision-screener/";

$lang['diagnostico_innovacion_title_04'] = "Cardiologia Diagnóstica";
$lang['diagnostico_innovacion_description_04'] = "Configure nossas soluções no Pulso";
$lang['diagnostico_innovacion_link_04'] = "https://www.hillrom.lat/pt/pulso/";

$lang['diagnostico_innovacion_todo'] = "https://www.hillrom.lat/pt/products/promotions/spot--vision-screener/";

$lang['title_innovacion'] = "Inovação em ação"; 
$lang['description_innovacion'] = "Conheça nossas mais recentes novidades"; 

$lang['title_solu_int'] = "Soluções hospitalares";
$lang['description_solu_int'] = "Descubra um mundo de opções inovadoras";

$lang['soluciones_lbl_ver_todo'] = "https://www.hillrom.lat/pt/products-category/surgical-workflow-and-precision-positioning/";

$lang['soluciones_title'] = "Sala de cirurgia otimizada, paciente seguro";
$lang['soluciones_description'] = "Nossas soluções versáteis e flexíveis otimizam a eficácia e o fluxo de trabalho nas salas de cirurgia, bem como a segurança do paciente.";

$lang['soluciones_product_01_title'] = "Mesas<br>de precisão";
$lang['soluciones_product_01_description'] = "Mesas cirúrgicas com posicionamento preciso para cada situação que permitem otimizar a sala cirúrgica e a assistência de outros aparelhos.";
$lang['soluciones_product_01_link'] = "https://www.hillrom.lat/pt/products-category/surgical-workflow-and-precision-positioning/surgical-tables/";

$lang['soluciones_product_02_title'] = "Lâmpadas<br>Inteligentes";
$lang['soluciones_product_02_description'] = "Eles garantem uma distribuição uniforme de luz na área cirúrgica e fornecem iluminação que antecipa e reage. ";
$lang['soluciones_product_02_link'] = "https://www.hillrom.lat/pt/products-category/surgical-workflow-and-precision-positioning/surgical-and-examination-lights/";

$lang['soluciones_product_03_title'] = "Torres<br>configuráveis";
$lang['soluciones_product_03_description'] = "Nossas torres vão para onde você quiser, quando quiser, oferecendo a flexibilidade e o conforto de que você precisa na sala de cirurgia, na UTI e além.";
$lang['soluciones_product_03_link'] = "https://www.hillrom.lat/pt/products-category/surgical-workflow-and-precision-positioning/equipment-booms/";


$lang['soluciones_innovacion_title_01'] = "Soluções cirúrgicas";
$lang['soluciones_innovacion_description_01'] = "Aqui, cada segundo conta";
$lang['soluciones_innovacion_link_01'] = "https://www.hillrom.lat/pt/solucoes-cirurgicas/";

$lang['soluciones_innovacion_title_02'] = "TORI";
$lang['soluciones_innovacion_description_02'] = "A Base do Cuidado Cirúrgico";
$lang['soluciones_innovacion_link_02'] = "https://www.hillrom.lat/pt/tori/";

$lang['cuidado_lbl_ver_todo'] = "https://www.hillrom.lat/pt/solutions/simplify-clinical-communication-and-collaboration/";

$lang['cuidado_title'] = "Simplifique a comunicação e promova a colaboração";
$lang['cuidado_description'] = "A plataforma digital Connecta auxilia na melhor tomada de decisão e permite monitoramento e atendimento proativo ao paciente.";

$lang['cuidado_product_01_title'] = "Exibição<br>máxima";
$lang['cuidado_product_01_description'] = "Fornece informações clínicas objetivas em uma única tela nas estações de trabalho e nos dispositivos da equipe de atendimento em cada ambiente hospitalar.";
$lang['cuidado_product_01_link'] = "https://www.hillrom.lat/pt/solutions/simplify-clinical-communication-and-collaboration/";
$lang['cuidado_product_01_img'] = 'sec-03-prod-06.png';

$lang['cuidado_product_02_title'] = "Dados<br>clínicos";
$lang['cuidado_product_02_description'] = "Coleta e centraliza dados clínicos de várias fontes e fornece comunicação bidirecional personalizável com o prontuário eletrônico do paciente.";
$lang['cuidado_product_02_link'] = "https://www.hillrom.lat/pt/solutions/simplify-clinical-communication-and-collaboration/";
$lang['cuidado_product_01_img'] = 'sec-03-prod-07.png';

$lang['cuidado_product_03_title'] = "Ótima<br>tomada de<br>decisão";
$lang['cuidado_product_03_description'] = "Promove o empoderamento da equipe assistencial e ajuda a priorizar as atividades de cuidado ao paciente.";
$lang['cuidado_product_03_link'] = "https://www.hillrom.lat/pt/solutions/simplify-clinical-communication-and-collaboration/";
$lang['cuidado_product_01_img'] = 'sec-03-prod-08.png';


$lang['cuidado_innovacion_title_01'] = "Connecta";
$lang['cuidado_innovacion_description_01'] = "Prepare-se para a nova revolução na saúde.";
$lang['cuidado_innovacion_link_01'] = "https://www.hillrom.lat/pt/connecta/";

$lang['cuidado_innovacion_title_02'] = "";
$lang['cuidado_innovacion_description_02'] = "";
$lang['cuidado_innovacion_link_02'] = "";


$lang['recuperacion_lbl_ver_todo'] = "https://www.hillrom.lat/pt/solutions/accelerate-patient-recovery/";

$lang['recuperacion_title'] = "Superando desafios, acelerando a recuperação";
$lang['recuperacion_description'] = "Superfícies e camas inteligentes que reduzem a prevalência de lesões e complicações pulmonares. Equipamento que ajuda a movimentar o paciente com segurança*.";

$lang['recuperacion_product_01_title'] = "Camas<br>inteligentes";
$lang['recuperacion_product_01_description'] = "Camas que respondem às necessidades do paciente e da equipe de atendimento; de alarmes a dados relevantes para a condição do paciente.";
$lang['recuperacion_product_01_link'] = "http://www.hillrom.lat/pt/progressa-plus/";

$lang['recuperacion_product_02_title'] = "Superficies";
$lang['recuperacion_product_02_description'] = "Temos uma grande variedade de modelos estáticos, mistos e dinâmicos, o que permite escolher a superfície perfeita de acordo com o nível de risco.";
$lang['recuperacion_product_02_link'] = "https://www.hillrom.lat/pt/products-category/smart-beds-and-surfaces/acute-care-surfaces/";
$lang['recuperacion_product_02_img'] = 'sec-04-prod-07-v2.png';

$lang['recuperacion_product_03_title'] = "Mobilidade";
$lang['recuperacion_product_03_description'] = "Equipamentos que viabilizam o transpo te e a mobilização segura do paciente enquanto avança em passos firmes em sua recuperação.";
$lang['recuperacion_product_03_link'] = "https://www.hillrom.lat/es/products-category/safe-patient-handling-and-mobility/";


$lang['recuperacion_innovacion_title_01'] = "Superfícies Hillrom";
$lang['recuperacion_innovacion_description_01'] = "Oferecemos o mais alto nível de superfícies médicas.";
$lang['recuperacion_innovacion_link_01'] = "https://www.hillrom.lat/pt/superficies-hillrom/";
$lang['recuperacion_innovacion_img_01'] = "back-super-hill.jpg";

$lang['recuperacion_innovacion_title_02'] = "";
$lang['recuperacion_innovacion_description_02'] = "";
$lang['recuperacion_innovacion_link_02'] = "";
$lang['recuperacion_innovacion_img_02'] = "";

$lang['recuperacion_ver_todo'] = 'https://www.hillrom.lat/pt/solutions/accelerate-patient-recovery/';


$lang['desc_foot'] = 'Baxter Hospitalar LTDA. Avenida Dr. Chucri Zaidan, N. 1.240 - Torre A, 18 andar. Vila São Francisco - São Paulo, SP - CEP:04711-13. Serviço ao cliente: 08000125522 - www.baxter.com.br . 2023 Baxter Hospitalar Ltda. Todos os direitos reservados. Baxter, Hillrom e Welch Allyn são marcas registradas da Baxter International Inc ou de suas subsidiárias. As demais marcas contidas neste material são de propriedade de seus respectivos detentores.<br><br>Para obter mais informações, consulte o representante de vendas local ou o especialista de produtos da Baxter.<br>
A disponibilidade de todos os nossos produtos, configurações e serviços são aplicadas de acordo com as aprovações de registros de cada país.';

$lang['desc_foot_flc'] = 'Baxter Hospitalar LTDA. Avenida Dr. Chucri Zaidan, N. 1.240 - Torre A, 18 andar. Vila São Francisco - São Paulo, SP - CEP:04711-13. Serviço ao cliente: 08000125522 - www.baxter.com.br . 2023 Baxter Hospitalar Ltda. Todos os direitos reservados. Baxter, Hillrom e Welch Allyn são marcas registradas da Baxter International Inc ou de suas subsidiárias. As demais marcas contidas neste material são de propriedade de seus respectivos detentores.<br><br>

					Numero de registro<br>
					Oftalmoscópio PanOptic - 80011689007 | Ostoscópio Macroview - 80011680025 | Transformador de Parede - 80011680079 | Escaneador Ocular - 80011680092
RScribe - 80145240454 | WAM -  80145240460<br><br>
					Para obter mais informações, consulte o representante de vendas local ou o especialista de produtos da Baxter. A disponibilidade de todos os nossos produtos, configurações e serviços são aplicadas de acordo com as aprovações de registros de cada país.';

$lang['desc_foot_gss'] = 'Baxter Hospitalar LTDA. Avenida Dr. Chucri Zaidan, N. 1.240 - Torre A, 18 andar. Vila São Francisco - São Paulo, SP - CEP:04711-13. Serviço ao cliente: 08000125522 - www.baxter.com.br . 2023 Baxter Hospitalar Ltda. Todos os direitos reservados. Baxter, Hillrom e Welch Allyn sao marcas registradas da Baxter International Inc ou de suas subsidiárias. As demais marcas contidas neste material sao de propriedade de seus respectivos detentores.<br><br>



Numero de registro<br>
PST500 - 80102512678 | TL1000 - 80102512687 | FCS 500 - 80102512703 | TS7500 - 80102512640 
<br><br>Para obter mais informações, consulte o representante de vendas local ou o especialista de produtos da Baxter.<br>
A disponibilidade de todos os nossos produtos, configurações e serviços são aplicadas de acordo com as aprovações de registros de cada país.';


$lang['desc_foot_connecta'] = 'Baxter Hospitalar LTDA. Avenida Dr. Chucri Zaidan, N. 1.240 - Torre A, 18 andar. Vila São Francisco - São Paulo, SP - CEP:04711-13. Serviço ao cliente: 08000125522 - www.baxter.com.br . 2023 Baxter Hospitalar Ltda. Todos os direitos reservados. Baxter, Hillrom e Welch Allyn sao marcas registradas da Baxter International Inc ou de suas subsidiárias. As demais marcas contidas neste material sao de propriedade de seus respectivos detentores.<br><br>



Numero de registro<br>
Connecta<br><br>Para obter mais informações, consulte o representante de vendas local ou o especialista de produtos da Baxter.<br>
A disponibilidade de todos os nossos produtos, configurações e serviços são aplicadas de acordo com as aprovações de registros de cada país.';


$lang['desc_foot_pss'] = 'Baxter Hospitalar LTDA. Avenida Dr. Chucri Zaidan, N. 1.240 - Torre A, 18 andar. Vila São Francisco - São Paulo, SP - CEP:04711-13. Serviço ao cliente: 08000125522 - www.baxter.com.br . 2023 Baxter Hospitalar Ltda. Todos os direitos reservados. Baxter, Hillrom e Welch Allyn são marcas registradas da Baxter International Inc ou de suas subsidiárias. As demais marcas contidas neste material são de propriedade de seus respectivos detentores.<br><br>



Numero de registro<br>
Progressa Smart Bed - 80102511447 | Superfície de Prevenção NP150 - 80102519034
<br><br>Para obter mais informações, consulte o representante de vendas local ou o especialista de produtos da Baxter.<br>
A disponibilidade de todos os nossos produtos, configurações e serviços são aplicadas de acordo com as aprovações de registros de cada país.';


$lang['new_info_home_box_01'] = 'MILHÕES';
$lang['new_info_home_box_01_desc'] = 'dos pacientes são tratados a cada ano com as inovações da Baxter.
Não importa em que estágio de cuidado você esteja, sempre estaremos lá para ajudá-lo a cuidar de seu paciente.';

$lang['new_info_home_box_02_desc'] = 'inovações da Baxter estão prontas para serem usadas para cuidar de seus pacientes em diferentes procedimentos hospitalares.';
$lang['new_info_home_box_03_desc'] = 'médicos aproximadamente, iniciam sua carreira médica com os dispositivos de diagnóstico da Welch Allyn.';

?>