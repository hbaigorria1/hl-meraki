<?php 

$lang['header'] = "Hillrom es parte de Baxter";
$lang['hl_link'] = "https://www.hillrom.lat/";
$lang['home'] = "https://www.hillrom.lat/juntos-por-la-salud/";

$lang['form_01'] = "https://pardot.hillrom.com/l/8232/2022-05-26/c5vf82";
$lang['form_02'] = "https://pardot.hillrom.com/l/8232/2022-07-18/cb2rm5";
$lang['form_03'] = "https://pardot.hillrom.com/l/8232/2022-07-18/cb2rly";
$lang['form_04'] = "https://pardot.hillrom.com/l/8232/2022-07-18/cb2rm2";
$lang['form_05'] = "https://pardot.hillrom.com/l/8232/2022-07-18/cb2rm8";

$lang['url_01'] = base_url()."/seccion/diagnostico-temprano/";
$lang['url_02'] = base_url()."/seccion/soluciones-quirurgicas/";
$lang['url_03'] = base_url()."/seccion/cuidado-conectado/";
$lang['url_04'] = base_url()."/seccion/recuperacion-del-paciente/";


$lang['video_01'] = "video-home-esp.mp4";
$lang['video_02'] = "22-HIL-0093 - Meraki GBU Videos_Connecta Video_Final Espaol_V2_1.mp4";
$lang['video_03'] = "22-HIL-0093 - Meraki GBU Videos_FLC Video_Final Espanol_V2_1.mp4";
$lang['video_04'] = "22-HIL-0093 - Meraki GBU Videos_PSS Video_Final Espanol_V3_1.mp4";
$lang['video_05'] = "22-HIL-0093 - Meraki GBU Videos_GSS Video_Final Espanol_V2_2.mp4";

$lang['title'] = 'Salvar y sostener vidas';
$lang['description'] = 'Esa es nuestra misión...<br>¡y la tuya! Descubre cómo nuestras soluciones de última generación te ayudan a hacerla realidad. Cada día. Todos los días.';


$lang['anios_01'] = 'AÑOS';
$lang['anios_02'] = 'de legado combinado entre nuestras marcas,<br>ofreciendo innovación médica enfocada<br>al cuidado del paciente.';
$lang['anios_03'] = '
<span>WELCH<br>ALLYN</span>
<span>MORTARA</span>
<span>HILLROM</span>
<span>TRUMPF<br>MEDICAL</span>
<span>ALLEN</span>';

$lang['title_01_principal'] = 'Somos Baxter.';
$lang['title_02'] = 'Somos tu mejor aliado';
$lang['description_02'] = "Con nuestras soluciones de última generación,<br>tú y tu hospital estarán mejor preparados<br>para cuidar la salud de los pacientes y responder<br>de una manera más proactiva, segura y efectiva.";

$lang['menu_01_title'] = "DIAGNÓSTICO PRIMARIO";
$lang['menu_01_description'] = "Mira la último en instrumentos de evaluación y verás cómo viabilizar un diagnóstico más temprano y preciso para el paciente.";

$lang['menu_02_title'] = "SOLUCIONES QUIRÚRGICAS";
$lang['menu_02_description'] = "Explora las innovadoras opciones para dinamizar el flujo de trabajo en la sala de cirugía y optimizar la seguridad del paciente.";

$lang['menu_03_title'] = "CUIDADO CONECTADO";
$lang['menu_03_description'] = "Experimenta un nuevo nivel de eficiencia e integración en la comunicación y colaboración del equipo de atención con nuestra avanzada plataforma digital Connecta.";

$lang['menu_04_title'] = "CUIDADO ESPECIALIZADO";
$lang['menu_04_description'] = "Conoce nuestras herramientas especialmente diseñadas para acelerar la recuperación del paciente en cualquier entorno hospitalario";

$lang['title_contacto'] = "Contáctanos";
$lang['description_contacto'] = "Para obtener más información o hablar directamente con un representante de ventas acerca de nuestros productos, soluciones y servicios, completa el siguiente formulario";

$lang['menu_section_01'] = "SOLUCIONES";
$lang['menu_section_02'] = "NOVEDADES";
$lang['menu_section_03'] = "EXPLORA";
$lang['menu_section_04'] = "CONTACTO";

$lang['lbl_ver_mas'] = "VER MÁS";
$lang['lbl_ver_todo'] = "VER TODO";

$lang['diagnostico_lbl_ver_todo'] = "https://www.hillrom.lat/es/solutions/enable-earlier-diagnosis-and-treatment/";

$lang['diagnostico_title'] = "Diagnóstico temprano, tratamiento oportuno";
$lang['diagnostico_description'] = "Nuestros dispositivos inteligentes e instrumentos digitales reducen los tiempos de evaluación y aumentan su precisión.";

$lang['diagnostico_product_01_title'] = "Examen Físico";
$lang['diagnostico_product_01_description'] = "Ahora podrás realizar las evaluaciones de detección de visión y monitoreo con mayor precisión y agilidad.";
$lang['diagnostico_product_01_link'] = "https://www.hillrom.lat/es/products/diagnostic-sets/";

$lang['diagnostico_product_02_title'] = "Cardiología";
$lang['diagnostico_product_02_description'] = "Con un legado de innovación de más de 40 años,
conoce nuestras soluciones de cardiología que
permiten evaluar, diagnosticar y tratar al
paciente con agilidad y confianza.";
$lang['diagnostico_product_02_link'] = "https://www.hillrom.lat/es/cardiologia-diagnostica";

$lang['diagnostico_product_03_title'] = "Monitoreo";
$lang['diagnostico_product_03_description'] = "Conectados a tu propósito, nuestras soluciones de monitoreo permiten la medición de signos vitales de manera precisa y eficiente.";
$lang['diagnostico_product_03_link'] = "https://www.hillrom.lat/es/products-category/vision-screening-and-diagnostics/";


$lang['diagnostico_title_innovacion'] = "Innovación en acción"; 
$lang['diagnostico_description_innovacion'] = "Descubre un mundo de opciones innovadoras"; 

$lang['diagnostico_innovacion_title_01'] = "Dispositivos de diagnóstico digital Welch Allyn";
$lang['diagnostico_innovacion_description_01'] = "Abre los ojos a la evaluación diagnóstica digital";
$lang['diagnostico_innovacion_link_01'] = "https://www.hillrom.lat/es/abre-los-ojos/";
$lang['diagnostico_innovacion_img_01'] = "images-09.png";

$lang['diagnostico_innovacion_title_02'] = "Pulso";
$lang['diagnostico_innovacion_description_02'] = "Cardiología Diagnóstica Configurable";
$lang['diagnostico_innovacion_link_02'] = "https://www.hillrom.lat/es/pulso/";
$lang['diagnostico_innovacion_img_02'] = "images-08.png";

$lang['diagnostico_innovacion_title_03'] = "Spot™ Vision Care";
$lang['diagnostico_innovacion_description_03'] = "Mira otro<br>futuro";
$lang['diagnostico_innovacion_link_03'] = "https://www.hillrom.lat/es/products/promociones/spot--vision-screener/";

$lang['diagnostico_innovacion_title_04'] = "Cardiología Diagnóstica";
$lang['diagnostico_innovacion_description_04'] = "Configura nuestras soluciones en Pulso";
$lang['diagnostico_innovacion_link_04'] = "https://www.hillrom.lat/es/pulso/";

$lang['diagnostico_innovacion_todo'] = "https://www.hillrom.lat/es/products/promociones/spot--vision-screener/";

$lang['title_solu_int'] = "Soluciones hospitalarias";
$lang['description_solu_int'] = "Conoce nuestras más recientes novedades";

$lang['title_innovacion'] = "Innovación en acción"; 
$lang['description_innovacion'] = "Conoce nuestras más recientes novedades"; 

$lang['soluciones_lbl_ver_todo'] = "https://www.hillrom.lat/es/solutions/optimize-surgical-efficiency-and-safety/";

$lang['soluciones_title'] = "Quirófano optimizado, paciente seguro";
$lang['soluciones_description'] = "Nuestras soluciones versátiles y flexibles optimizan tanto la efectividad y el flujo de trabajo en las salas de cirugía como la seguridad de los pacientes.";

$lang['soluciones_product_01_title'] = "Mesas de Posicionamiento Preciso";
$lang['soluciones_product_01_description'] = "Mesas quirúrgicas para cada situación que te permiten optimizar la sala de cirugía y la asistencia de otros dispositivos.";
$lang['soluciones_product_01_link'] = "https://www.hillrom.lat/es/products-category/surgical-workflow-and-precision-positioning/surgical-tables/";

$lang['soluciones_product_02_title'] = "Lámparas<br>Inteligentes";
$lang['soluciones_product_02_description'] = "Aseguran la distribución uniforme de luz en el área quirúrgica y proveen iluminación que se anticipa y reacciona.";
$lang['soluciones_product_02_link'] = "https://www.hillrom.lat/es/products-category/surgical-workflow-and-precision-positioning/surgical-and-examination-lights/";

$lang['soluciones_product_03_title'] = "Torres<br>configurables";
$lang['soluciones_product_03_description'] = "Nuestras torres van a donde deseas en el momento que quieras, brindándote la flexibilidad y la comodidad que necesitas en el quirófano, la UCI y mucho más.";
$lang['soluciones_product_03_link'] = "https://www.hillrom.lat/es/products-category/surgical-workflow-and-precision-positioning/equipment-booms/";



$lang['soluciones_innovacion_title_01'] = "Soluciones Quirúrgicas";
$lang['soluciones_innovacion_description_01'] = "Aquí, cada segundo cuenta";
$lang['soluciones_innovacion_link_01'] = "https://www.hillrom.lat/soluciones-quirurgicas/";

$lang['soluciones_innovacion_title_02'] = "TORI";
$lang['soluciones_innovacion_description_02'] = "La base de la atención quirúrgica";
$lang['soluciones_innovacion_link_02'] = "https://www.hillrom.lat/tori/";


$lang['cuidado_lbl_ver_todo'] = "https://www.hillrom.lat/es/solutions/accelerate-patient-recovery/";

$lang['cuidado_title'] = "Simplificar la comunicación y promover la colaboración";
$lang['cuidado_description'] = "La plataforma digital Connecta apoya una mejor toma de decisiones  y viabiliza el monitoreo y la atención proactiva del paciente.";

$lang['cuidado_product_01_title'] = "Máxima<br>visualización";
$lang['cuidado_product_01_description'] = "Provee información clínica objetiva en una sola pantalla tanto en las estaciones de trabajo como en los dispositivos del equipo de atención de cada entorno hospitalario.";
$lang['cuidado_product_01_link'] = "https://www.hillrom.lat/es/solutions/simplify-clinical-communication-and-collaboration/";
$lang['cuidado_product_01_img'] = 'new-image-08.png';

$lang['cuidado_product_02_title'] = "Datos<br>clínicos";
$lang['cuidado_product_02_description'] = "Recopila y centraliza los datos clínicos de múltiples fuentes y provee comunicación bidireccional personalizable con el registro electrónico del paciente.";
$lang['cuidado_product_02_link'] = "https://www.hillrom.lat/es/solutions/simplify-clinical-communication-and-collaboration/";
$lang['cuidado_product_02_img'] = 'new-image-09.png';

$lang['cuidado_product_03_title'] = "Óptima toma<br>de decisión";
$lang['cuidado_product_03_description'] = "Promueve el empoderamiento del equipo de atención y ayuda a priorizar las actividades para el cuidado del paciente.";
$lang['cuidado_product_03_link'] = "https://www.hillrom.lat/es/solutions/simplify-clinical-communication-and-collaboration/";
$lang['cuidado_product_03_img'] = 'new-image-10.png';


$lang['cuidado_innovacion_title_01'] = "Connecta";
$lang['cuidado_innovacion_description_01'] = "Prepárate para la nueva revolución en el cuidado de la salud";
$lang['cuidado_innovacion_link_01'] = "https://www.hillrom.lat/es/connecta/";

$lang['cuidado_innovacion_title_02'] = "Connecta Talks";
$lang['cuidado_innovacion_description_02'] = "Impulsando el éxito en la transformación digital en tu hospital";
$lang['cuidado_innovacion_link_02'] = "https://www.hillrom.lat/es/connecta-talks/";


$lang['recuperacion_lbl_ver_todo'] = "https://www.hillrom.lat/es/solutions/accelerate-patient-recovery/";

$lang['recuperacion_title'] = "Superando desafíos, acelerando la recuperación";
$lang['recuperacion_description'] = "Superficies y camas inteligentes que permiten reducir tanto la prevalencia de lesiones como las complicaciones pulmonares. Equipos que ayudan a la movilización del paciente de manera segura.";

$lang['recuperacion_product_01_title'] = "Camas<br>inteligentes";
$lang['recuperacion_product_01_description'] = "Camas que responden a las necesidades del paciente y del personal de cuidado; desde alarmas hasta datos pertinentes al estado del paciente.";
$lang['recuperacion_product_01_link'] = "http://www.hillrom.lat/es/progressa-plus/";

$lang['recuperacion_product_02_title'] = "Superficies";
$lang['recuperacion_product_02_description'] = "Contamos con una amplia variedad de modelos estáticos, mixtos y dinámicos, lo que te permite elegir la superficie perfecta de acuerdo al nivel de riesgo.";
$lang['recuperacion_product_02_link'] = "https://www.hillrom.lat/es/products-category/smart-beds-and-surfaces/acute-care-surfaces/";
$lang['recuperacion_product_02_img'] = 'new-image-12.png';


$lang['recuperacion_product_03_title'] = "Movilidad";
$lang['recuperacion_product_03_description'] = "Equipos que permiten la movilización segura del paciente mientras avanza a paso ﬁrme en su recuperación";
$lang['recuperacion_product_03_link'] = "https://www.hillrom.lat/es/products-category/safe-patient-handling-and-mobility/";


$lang['recuperacion_innovacion_title_01'] = "Evoluciona";
$lang['recuperacion_innovacion_description_01'] = "El cuidado intensivo con Baxter";
$lang['recuperacion_innovacion_link_01'] = "https://www.hillrom.lat/es/evoluciona/";
$lang['recuperacion_innovacion_img_01'] = "image-122.png";

$lang['recuperacion_innovacion_title_02'] = "Superﬁcies Hillrom";
$lang['recuperacion_innovacion_description_02'] = "Ofrecemos el más alto nivel de superﬁcies médicas";
$lang['recuperacion_innovacion_link_02'] = "https://www.hillrom.lat/es/catalogodesuperficies/";
$lang['recuperacion_innovacion_img_02'] = "image-123.png";

$lang['recuperacion_ver_todo'] = 'https://www.hillrom.lat/es/solutions/optimize-surgical-efficiency-and-safety/';

$lang['desc_foot'] = 'Para más información consulte con su representante de venta local o especialista de Producto Baxter.<br> La disponibilidad y restricciones de todos nuestros productos, configuraciones y servicios se aplican de acuerdo con los registros disponibles de cada país.';

$lang['desc_foot_flc'] = 'Para más información consulte con su representante de venta local o especialista de Producto Baxter.<br> La disponibilidad y restricciones de todos nuestros productos, configuraciones y servicios se aplican de acuerdo con los registros disponibles de cada país.';

$lang['desc_foot_gss'] = 'Para más información consulte con su representante de venta local o especialista de Producto Baxter.<br> La disponibilidad y restricciones de todos nuestros productos, configuraciones y servicios se aplican de acuerdo con los registros disponibles de cada país.';

$lang['desc_foot_connecta'] = 'Para más información consulte con su representante de venta local o especialista de Producto Baxter.<br> La disponibilidad y restricciones de todos nuestros productos, configuraciones y servicios se aplican de acuerdo con los registros disponibles de cada país.';

$lang['desc_foot_pss'] = 'Para más información consulte con su representante de venta local o especialista de Producto Baxter.<br> La disponibilidad y restricciones de todos nuestros productos, configuraciones y servicios se aplican de acuerdo con los registros disponibles de cada país.';



$lang['new_info_home_box_01'] = 'MILLONES';
$lang['new_info_home_box_01_desc'] = 'De pacientes son tratados cada año con innovaciones médicas de Baxter. No importa en que etapa del cuidado se encuentre, siempre estaremos ahí para ayudarte a atender a tu paciente.';

$lang['new_info_home_box_02_desc'] = 'innovaciones de Baxter están listas para ser utilizadas para atender a tus pacientes en diferentes procedimientos hospitalarios.';
$lang['new_info_home_box_03_desc'] = 'médicos, se estima, comienzan su carrera en medicina con dispositivos de diagnóstico Welch Allyn.';



?>